function varargout = PL_test0(varargin)
% PL_test0 MATLAB code for PL_test0.fig
%      PL_test0, by itself, creates a new PL_test0 or raises the existing
%      singleton*.
%
%      H = PL_test0 returns the handle to a new PL_test0 or the handle to
%      the existing singleton*.
%
%      PL_test0('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PL_test0.M with the given input arguments.
%
%      PL_test0('Property','Value',...) creates a new PL_test0 or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PL_test0_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PL_test0_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PL_test0

% Last Modified by GUIDE v2.5 27-May-2014 14:55:52

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PL_test0_OpeningFcn, ...
                   'gui_OutputFcn',  @PL_test0_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});      %%error_msg
end
% End initialization code - DO NOT EDIT

% --- Executes just before PL_test0 is made visible.
function PL_test0_OpeningFcn(hObject, eventdata, handles, varargin)
 handles.output = hObject;
 guidata(hObject, handles);

% UIWAIT makes PL_test0 wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
 function varargout = PL_test0_OutputFcn(hObject, eventdata, handles)
 varargout{1} = handles.output;
 
 
% --- Executes on button press in intensity_plot.
function intensity_plot_Callback(hObject, eventdata, handles)
x=handles.x; y=handles.y; 
x_unique = handles.x_unique; y_unique=handles.y_unique;
colbar = handles.colbar;  data_scal = handles.data_scal;
        wvl_idx = min(find(abs(handles.wavelength-get (handles.slider1, 'Value'))<10^-0));

        zz1 = handles.intensity(wvl_idx,:)';

        M=zeros(length(y_unique),length(x_unique));
        for i=1:length(zz1)
            xi=find(y_unique==y(i));
            yi=find(x_unique==x(i));
            M(xi,yi)=zz1(i);
        end
        axes(handles.axes1);
        cla;   
        %M = flipud(M);       % flip up to dn, change ytick from -100:75 to 75:-100
        if data_scal == 1
            M=log(M);
            M(M<0)=0;  % avoid -Inf
        end        
        if colbar == 0
            M(M == 0) = NaN;
            imagescwithnan(M,hot,[0.827 0.827 0.827]);   % change NaN to white color
        end
        imagesc(x_unique, y_unique, M)
        colorbar;
        set(gca,'XTick',x_unique);
        set(gca,'YTick',y_unique);
        %yticklabels = y_unique;   % flip ylable
        %set(gca,'YTick',y_unique,'YTickLabel',flipud(yticklabels(:)));


% --- Executes on button press in peak_pos_plot.
function peak_pos_plot_Callback(hObject, eventdata, handles)
x=handles.x; y=handles.y; 
x_unique = handles.x_unique; y_unique=handles.y_unique;
colbar = handles.colbar;  data_scal = handles.data_scal;
        M = handles.M_peak;
        %M = flipud(M);       % flip up to dn, change ytick from -100:75 to 75:-100
        if data_scal == 1
            M=log(M);
            M(M<0)=0;  % avoid -Inf  same as set(colorbar,'YScale','log');
        end
        if colbar == 0
            M(M == 0) = NaN;
            imagescwithnan(M,hot,[0.827 0.827 0.827]);   % change NaN to white color
        end
        axes(handles.axes1); cla;  
        imagesc(x_unique, y_unique, M)
        colorbar;

        set(gca,'XTick',x_unique);
        set(gca,'YTick',y_unique);
        %yticklabels = y_unique;   % flip ylable
        %set(gca,'YTick',y_unique,'YTickLabel',flipud(yticklabels(:)));


% --- Executes on button press in FWHM.
function FWHM_Callback(hObject, eventdata, handles)
x=handles.x; y=handles.y; 
x_unique = handles.x_unique; y_unique=handles.y_unique;
colbar = handles.colbar;  data_scal = handles.data_scal;
        M = handles.M_FWHM;
        axes(handles.axes1);
        cla;   
        %M = flipud(M);       % flip up to dn, change ytick from -100:75 to 75:-100
        if data_scal == 1
            M=log(M);
            M(M<0)=0;  % avoid -Inf
        end   
        if colbar == 0
            M(M == 0) = NaN;
            imagescwithnan(M,hot,[0.827 0.827 0.827]);   % change NaN to white color
        end
        imagesc(x_unique, y_unique, M)
        colorbar;
        set(gca,'XTick',x_unique);
        set(gca,'YTick',y_unique);
        %yticklabels = y_unique;   % flip ylable
        %set(gca,'YTick',-100:25:75,'YTickLabel',flipud(yticklabels(:)));


% % --------------------------------------------------------------------
% function OpenMenuItem_Callback(hObject, eventdata, handles)
% file = uigetfile('*.fig');
% if ~isequal(file, 0)
%     open(file);
% end
% 
% % --------------------------------------------------------------------
% function PrintMenuItem_Callback(hObject, eventdata, handles)
% printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)


% --- Executes on button press in LoadConfigFileButton.
function LoadConfigFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadConfigFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FilenameConfigFile, PathnameConfigFile] = uigetfile({'*.csv'},'Select your config file');
FullPathName = fullfile (PathnameConfigFile, FilenameConfigFile);
set(handles.ConfigFileTextbox, 'String', FullPathName); % showing FullpathName


fid = fopen(FullPathName);
temp = textscan(fid, '%f %f %f', 'HeaderLines',11, 'Delimiter', ',','CollectOutput', 1);
fclose(fid);
position = temp{1};
assignin('base', 'position',position);
handles.position=position;
guidata(hObject, handles);


% --- Executes on button press in LoadDataFilesButton.
function LoadDataFilesButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadDataFilesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fileNameDataFiles, pathNameDataFiles] = uigetfile({'*.csv'},'Select your data files', 'MultiSelect' ,'on');

if iscell(fileNameDataFiles)
    nbfiles = length(fileNameDataFiles);
elseif fileNameDataFiles ~= 0
    nbfiles = 1;
else
    nbfiles = 0;
end
handles.nbfiles = nbfiles;

fullPathNameDataFiles = fullfile (pathNameDataFiles, fileNameDataFiles);

intensity = []; %initialize data  %%%%LY: ill defined, should include length
wavelength = [];
h2 = waitbar(0,'Data Loading...');
for i=1:nbfiles
temp = csvread(fullPathNameDataFiles{i},17,0); %we need the {} because it is a cell and not a matrix
intensity(:,i) = temp (:,2);
wavelength(:,1) = temp (:,1);
waitbar(i/nbfiles,h2)
end
close(h2);

maxI=max(intensity(:));

wavelengthStep=wavelength(2)-wavelength(1);
wavelengthRange=max(wavelength)-min(wavelength);

set(handles.slider1,...
      'max',max(wavelength),'min',min(wavelength),'Value',min(wavelength),...
      'SliderStep', [wavelengthStep/(wavelengthRange) 10*wavelengthStep/(wavelengthRange)]);
  
axes(handles.axes2);
cla;
plot(wavelength,intensity(:,1),'LineWidth',2);
set(gca,'ytick',[]);
axis([min(wavelength) max(wavelength) 0 max(intensity(:,1))]);
hold on;


handles.wavelength=wavelength;
handles.intensity=intensity;
handles.maxI=maxI;

%%%%%%%%%%%%%%%%% Analysis Part %%%%%%%%%%%%%%%%%

        [nbrRow nbrColumn] = size(handles.intensity);
        z=zeros(nbrRow,1);
        wave_x=handles.wavelength;  % rename wave_x
        peakPosition=zeros(length(handles.position),1);
        FWHM=zeros(length(handles.position),1);
        % should make general variable accessible to all LY
         x = handles.position(:,2); y = handles.position(:,3);        
         x_unique = sort(unique(x)); y_unique = sort(unique(y));
         handles.x = x; handles.y =y;
         handles.x_unique = x_unique; handles.y_unique= y_unique;
         
        fo = fitoptions('method','NonlinearLeastSquares',...
            'Lower',[0, min(wave_x), 0],...
            'Upper',[handles.maxI, max(wave_x), 5] );
        fit_t=fittype('gauss1');
        h1 = waitbar(0,'Please wait, data processing...');
        for i = 1:nbrColumn
            z=handles.intensity(:,i)-median(handles.intensity(:,i)); 
            % get ride of background 
            cf=fit(wave_x,z,fit_t,fo);   
            coeff_fit=coeffvalues(cf);
            peakPosition(i)=coeff_fit(2);  
            FWHM(i)=coeff_fit(3)*sqrt(2*log(2)); %%why??
            waitbar(i/nbrColumn,h1)
        end
        close(h1);
        
        z=peakPosition;
        M_peak=zeros(length(y_unique),length(x_unique));
        for i=1:length(z)
            xi=find(y_unique==y(i));
            yi=find(x_unique==x(i));
            M_peak(xi,yi)=z(i);
        end
   
        handles.M_peak = M_peak;          
        
        M_FWHM=zeros(length(y_unique),length(x_unique));
        for i=1:length(FWHM)
            xi=find(y_unique==y(i));
            yi=find(x_unique==x(i));
            M_FWHM(xi,yi)=FWHM(i);
        end
        
        handles.M_FWHM = M_FWHM;
        
guidata(hObject, handles);



% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
set(handles.wavelengthTextBox,'String',sprintf('%.4f nm',get (handles.slider1, 'Value')));
wvl_idx = min(find(abs(handles.wavelength-get (handles.slider1, 'Value'))<10^-0));

%Plot the vertical line on the small plot for reference
selectedWavelength = get (handles.slider1, 'Value');
axes(handles.axes2);
cla;
plot(handles.wavelength,log(handles.intensity(:,1)),'LineWidth',2);       %%% error msg
set(gca,'ytick',[]);
axis([min(handles.wavelength) max(handles.wavelength) log(handles.maxI/4) log(handles.maxI+1)]);
hold on;
x1=[selectedWavelength,selectedWavelength];
y1=[0,handles.maxI];
plot(x1,y1);


x = handles.position(:,2);y = handles.position(:,3);
x_unique = sort(unique(x));y_unique = sort(unique(y));

position_range = (x_unique(2)-x_unique(1));
handles.position_range = position_range;
guidata(hObject, handles);

% %%%% instant Plot as slide bar changes; scale wrt all wavelength

        zz1 = handles.intensity(wvl_idx,:)';

        M=zeros(length(y_unique),length(x_unique));
        for i=1:length(zz1)
            xi=find(y_unique==y(i));
            yi=find(x_unique==x(i));
            M(xi,yi)=zz1(i);
        end
        axes(handles.axes1);
        cla;   
        %M = flipud(M);       % flip up to dn, change ytick from -100:75 to 75:-100
        M(M == 0) = NaN;
        imagescwithnan(M,hot,[0.827 0.827 0.827]);   % change NaN to white color
        imagesc(x_unique, y_unique, M)
        colorbar;
        set(gca,'XTick',x_unique);
        set(gca,'YTick',y_unique);
        %yticklabels = y_unique;   % flip ylable
        %set(gca,'YTick',y_unique,'YTickLabel',flipud(yticklabels(:)));
        

handles.wvl_idx = wvl_idx;
guidata(hObject, handles);



% --- Executes on button press in data_acquire.
function data_acquire_Callback(hObject, eventdata, handles)
x = handles.position(:,2);y = handles.position(:,3);
x_unique = sort(unique(x));y_unique = sort(unique(y));
position = handles.position;
intensity = handles.intensity;
wavelength = handles.wavelength;

cursor_pos = ginput(1);      % cooridnate location
position_range = (x_unique(2)-x_unique(1));
data_pos   = position_range*[floor((cursor_pos(1)+(0.5*position_range))/position_range),floor((cursor_pos(2)+0.5*position_range)/position_range)];

% if sum(x_unique==data_pos(1)) == 0 && sum(y_unique==data_pos(2)) == 0
%     set(handles.text_coord,'String',{'No data in range'});
%     
% else
index = position(x==data_pos(1) & y==data_pos(2)); % index for intensity col 
if index <= size(intensity,2)
ity_y = intensity(:,index);
% plot(ity_x, ity_y) % plot intensity at certain position 
set(handles.text_coord,'String',{[' x = ',num2str(data_pos(1)),',  y = ',num2str(data_pos(2))]});
% set(handles.text_coord,'String',{[' x = ',num2str(data_pos(1)),',  y = ',num2str(data_pos(2))],...
%     ['data max wavelength=',num2str(wavelength(find(ity_y==max(ity_y))))]});
%plot(gaussfit, ity_x, ity_y)
figure();
% gauss 4 fir, time consuming
% gaussfit = fit(wavelength,ity_y,'gauss4');
% plot(gaussfit, wavelength, ity_y);
fo = fitoptions('method','NonlinearLeastSquares');
fit_t=fittype('gauss1');

ity_y = ity_y-median(ity_y); % substracte background
cf = fit(wavelength, ity_y, fit_t, fo);
plot(cf, wavelength, ity_y);
ylim([mean(ity_y)-2*std(ity_y),max(ity_y)+0.05*(max(ity_y)-mean(ity_y)+2*std(ity_y))]);
coeffs  = coeffvalues(cf);         % extract fit coefficients
coeff_2 = coeffs(2);               % peak 
title({'Intensity Plot',[' x = ',num2str(data_pos(1)),',  y = ',num2str(data_pos(2))],...
    ['Gaussfit Peak x =', num2str(coeff_2)]})
legend('Location','NorthWest');
else
    set(handles.text_coord,'String',{'No data in range'});
end




% --- Executes when selected object is changed in colbar.
function colbar_SelectionChangeFcn(hObject, eventdata, handles)
Buttons = get(eventdata.NewValue,'Tag');
colbar = 0;
switch Buttons
    case 'auto_col'
        colbar = 0;
    case 'zero_col'
        colbar = 1;
end
handles.colbar = colbar;
guidata(hObject, handles);



% --- Executes when selected object is changed in data_scal.
function data_scal_SelectionChangeFcn(hObject, eventdata, handles)
    Buttons = get(eventdata.NewValue,'Tag');
data_scal = 0;
switch Buttons
    case 'linear_scal'
        data_scal = 0;
    case 'log_scal'
        data_scal = 1;
end
handles.data_scal = data_scal;
guidata(hObject, handles);
