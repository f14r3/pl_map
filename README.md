# README #

### What is this repository for? ###

This is the MATLAB program intend to map the Photoluminescence spectrum on the wafer with GUI interface developed for SMART. 
Main function includes Intensity Plot vs Wavelength, PL Peak finding, Mapping Peak values to wafer with FWHM.
Colorbar are offered in linear scale and log scale.  Data range comes with global min to max or local data min to max.
The "Acquire Data" enable user to read the PL spectrum of certain point in the wafer and show fitted curve in the same time.

### Quick summary ###

* Version 1.4

Stable. Test on different size of wafer size conducted. 
GUI redesigned. 

* Version 0.4

Main function like Intensity Plot, Peak Position Mapping and FWHM Plot were achieved. 
Fast algorithm developed only one loop for data processing. 
  
### GUI review  ###
![InGaP-main.png](https://bitbucket.org/repo/RMGXRR/images/107721473-InGaP-main.png)
![InGaP-plot.png](https://bitbucket.org/repo/RMGXRR/images/3034548584-InGaP-plot.png)

### How do I get set up? ###

* Environment

  Matlab 2013a
 
* Dependencies

  PL_test0.m PL_test.fig imagescwithnan.m

* Database configuration

  The database was not constructed. Data(.csv) are all installed in a folder.

* Deployment instructions

### Contribution guidelines ###

* Writing tests

  test.m is for main function realization. you can play with it by loading data.mat

* Code review


### Who do I talk to? ###

* Repo admin
