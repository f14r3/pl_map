clear all 
close all
load('workspace good data.mat');
clc;

%Case 1 is the choosen wavelength line
wvl_idx=700;

x = position(:,2);
y = position(:,3);
z = intensity(wvl_idx,:)';

x_unique = sort(unique(x));
y_unique = sort(unique(y));

M = zeros(length(y_unique),length(x_unique)); % order checked
for i=1:length(z)
   xi=find(y_unique==y(i));
   yi=find(x_unique==x(i));
   M(xi,yi)=z(i);
end                          
figure(1); %fig1 = figure(1);
M = flipud(M);       % flip up to dn, change ytick from -100:75 to 75:-100
M(M == 0) = NaN;
imagescwithnan(M,hot,[1 1 1]);   % change NaN to white color
imagesc(x_unique, y_unique, M)   % plot use imagesc
%the fixed color range using 
%imagesc(X,[minval,maxval]);

set(gca,'XTick',-75:25:75);
yticklabels = -100:25:75;   % flip ylable
set(gca,'YTick',-100:25:75,'YTickLabel',flipud(yticklabels(:)));
%set(gca,'xtick',-75:25:75,'ytick',-100:25:75);
colorbar
% trial 1
% set(colorbar,'YScale','log');  %the colorbar to be in log scale

% trial 2
%TL = get(colorbar, 'YTick');
%set(colorbar, 'YTickLabel', sprintf('10^%g|', TL));
%set(colorbar, 'YTickLabel', sprintf('%g|', exp(TL)))
set (figure(1), 'Units', 'normalized', 'Position', [0 0.3 0.4 0.4]);


cursor_pos = ginput(1);          % cooridnate location
data_pos   = 25*[floor((cursor_pos(1)+12.5)/25),floor((cursor_pos(2)+12.5)/25)];
% convert cursor location to data position
index = position(x==data_pos(1) & y==data_pos(2)) % index for intensity col 
% index =  position(position(:,2)==-50 & position(:,3)==75)

figure(2);
ity_y = intensity(:,index);
ity_x = [1:size(ity_y)];           % A.U. ? or xx = linspace(0,10,length(ity_y))
% plot(ity_x, ity_y) % plot intensity at certain position 
gaussfit = fit(ity_x',ity_y,'gauss4') % fourth order fit % cftool(ity_x,ity_y) for visual
plot(gaussfit, ity_x, ity_y)
coeffs  = coeffvalues(gaussfit);         % extract fit coefficients
coeff_2 = coeffs(2);
title({'Intensity Plot',[' x = ',num2str(data_pos(1)),',  y = ',num2str(data_pos(2))],...
    ['4th order Gaussfit Peak x =', num2str(coeff_2)]})
set (figure(2), 'Units', 'normalized', 'Position', [0.5 0.3 0.4 0.4]);
coeffs(3)



figure(3); %%%%%% gauss1 with Nonlinear Least Square 

wave_x=wavelength;  % rename wave_x
fo = fitoptions('method','NonlinearLeastSquares');
% ,...
%             'Lower',[0, min(wave_x), 0],...
%             'Upper',[max(intensity(:)), max(wave_x), 5] );
fit_t=fittype('gauss1');
z=intensity(:,index)-median(intensity(:,index));   %% LY: mean take care of bkg
cf = fit(wave_x,z,fit_t,fo);
coeffs  = coeffvalues(cf); 
coeffs(3) %FWHM display
coeffs(2) %Peak center display 
% FWHM for gauss4 is difficult to calculate

plot(cf, wave_x, z)




