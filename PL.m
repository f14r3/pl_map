function varargout = PL(varargin)
% PL MATLAB code for PL.fig
%      PL, by itself, creates a new PL or raises the existing
%      singleton*.
%
%      H = PL returns the handle to a new PL or the handle to
%      the existing singleton*.
%
%      PL('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PL.M with the given input arguments.
%
%      PL('Property','Value',...) creates a new PL or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PL_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PL_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PL

% Last Modified by GUIDE v2.5 10-Apr-2014 11:47:27

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PL_OpeningFcn, ...
                   'gui_OutputFcn',  @PL_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT

% --- Executes just before PL is made visible.
function PL_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PL (see VARARGIN)

%Change the colorbar
 CT=cbrewer('seq', 'YlOrRd', 128);
handles.cmap=colormap(CT);



% Choose default command line output for PL
handles.output = hObject;

% Update handles structure
guidata(hObject, handles);



% UIWAIT makes PL wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PL_OutputFcn(hObject, eventdata, handles)
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;

% --- Executes on button press in pushbutton1.
function pushbutton1_Callback(hObject, eventdata, handles)
% hObject    handle to pushbutton1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
axes(handles.axes1);
cla;

popup_sel_index = get(handles.listbox1, 'Value');
switch popup_sel_index
    case 1 %Intensity Vs wavelength
        
        %find the maximum peak intensity
        maxI=max(handles.intensity(:));
        
        
        
    case 2 %Plot the peak wavelength
        clc;
        [nbrRow nbrColumn] = size(handles.intensity);
        z=zeros(nbrRow,1);
        x=handles.wavelength;
        peakPosition=zeros(length(handles.position),1);
        FWHM=zeros(length(handles.position),1);
        fo_ = fitoptions('method','NonlinearLeastSquares',...
            'Lower',[0, min(x), 0],...
            'Upper',[handles.maxI, max(x), 5] );
        fit_t=fittype('gauss1');
        h1 = waitbar(0,'Please wait, computing...');
        for i = 1:nbrColumn
            z=handles.intensity(:,i);
%             [Y, I]=max(z);
%             peakPosition(i)=handles.wavelength(I);
%             P=findpeaks(x,z,1e-2,10,6,7,3); %Method using findpeaks
%             script
%             peakPosition(i)=P(1,2);
%             FWHM(i)=P(1,4);
            cf=fit(x,z,fit_t,fo_);
            coeff_fit=coeffvalues(cf);
            peakPosition(i)=coeff_fit(2);
            FWHM(i)=coeff_fit(3)*sqrt(2*log(2));
            waitbar(i/nbrColumn,h1)

        end
        close(h1);
        x = handles.position(:,2);
y = handles.position(:,3);
z=peakPosition;


x_unique = sort(unique(x));
y_unique = sort(unique(y));


    M=zeros(length(y_unique),length(x_unique));
    for i=1:length(z)
   xi=find(y_unique==y(i));
   yi=find(x_unique==x(i));
   M(xi,yi)=z(i);
    end
    axes(handles.axes1);
cla;    
% mesh(x_unique,y_unique,M);
% pcolor(x_unique,y_unique,M);
% shading flat;
scatter(x,y,5000,z,'square','filled')
colorbar;
axis equal;
% caxis([min(peakPosition) max(peakPosition)]);

 
handles.FWHM=FWHM;
handles.peakPosition=peakPosition;
guidata(hObject, handles);

    case 3
x = handles.position(:,2);
y = handles.position(:,3);
z=handles.FWHM;


x_unique = sort(unique(x));
y_unique = sort(unique(y));


    M=zeros(length(y_unique),length(x_unique));
    for i=1:length(z)
   xi=find(y_unique==y(i));
   yi=find(x_unique==x(i));
   M(xi,yi)=z(i);
    end
    axes(handles.axes1);
cla;    
mesh(x_unique,y_unique,M);
pcolor(x_unique,y_unique,M);
shading flat;
colorbar;
axis equal;
caxis([min(z) max(z)]);

end


% --------------------------------------------------------------------
function FileMenu_Callback(hObject, eventdata, handles)
% hObject    handle to FileMenu (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)


% --------------------------------------------------------------------
function OpenMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to OpenMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
file = uigetfile('*.fig');
if ~isequal(file, 0)
    open(file);
end

% --------------------------------------------------------------------
function PrintMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to PrintMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
printdlg(handles.figure1)

% --------------------------------------------------------------------
function CloseMenuItem_Callback(hObject, eventdata, handles)
% hObject    handle to CloseMenuItem (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
selection = questdlg(['Close ' get(handles.figure1,'Name') '?'],...
                     ['Close ' get(handles.figure1,'Name') '...'],...
                     'Yes','No','Yes');
if strcmp(selection,'No')
    return;
end

delete(handles.figure1)




% --- Executes on button press in LoadConfigFileButton.
function LoadConfigFileButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadConfigFileButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[FilenameConfigFile, PathnameConfigFile] = uigetfile({'*.csv'},'Select your config file');
FullPathName = fullfile (PathnameConfigFile, FilenameConfigFile);
set(handles.ConfigFileTextbox, 'String', FullPathName); % showing FullpathName


fid = fopen(FullPathName);
temp = textscan(fid, '%f %f %f', 'HeaderLines',11, 'Delimiter', ',','CollectOutput', 1);
fclose(fid);
position = temp{1};
assignin('base', 'position',position);
handles.position=position;
guidata(hObject, handles);



% --- Executes on button press in LoadDataFilesButton.
function LoadDataFilesButton_Callback(hObject, eventdata, handles)
% hObject    handle to LoadDataFilesButton (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
[fileNameDataFiles, pathNameDataFiles] = uigetfile({'*.csv'},'Select your data files', 'MultiSelect' ,'on');

if iscell(fileNameDataFiles)
    nbfiles = length(fileNameDataFiles);
elseif fileNameDataFiles ~= 0
    nbfiles = 1;
else
    nbfiles = 0;
end
handles.nbfiles = nbfiles;

fullPathNameDataFiles = fullfile (pathNameDataFiles, fileNameDataFiles);

intensity = []; %initialize data
wavelength = [];
for i=1:nbfiles
temp = csvread(fullPathNameDataFiles{i},17,0); %we need the {} because it is a cell and not a matrix
intensity(:,i) = temp (:,2);
wavelength(:,1) = temp (:,1);
end

maxI=max(intensity(:));

wavelengthStep=wavelength(2)-wavelength(1);
wavelengthRange=max(wavelength)-min(wavelength);

set(handles.slider1,...
      'max',max(wavelength),'min',min(wavelength),'Value',min(wavelength),...
      'SliderStep', [wavelengthStep/(wavelengthRange) 10*wavelengthStep/(wavelengthRange)]);
  
axes(handles.axes2);
cla;
plot(wavelength,intensity(:,1),'LineWidth',2);
set(gca,'ytick',[]);
axis([min(wavelength) max(wavelength) 0 max(intensity(:,1))]);
hold on;


  


handles.wavelength=wavelength;
handles.intensity=intensity;
handles.maxI=maxI;
guidata(hObject, handles);


% --- Executes on slider movement.
function slider1_Callback(hObject, eventdata, handles)
set(handles.wavelengthTextBox,'String',sprintf('%.4f nm',get (handles.slider1, 'Value')));
wvl_idx = min(find(abs(handles.wavelength-get (handles.slider1, 'Value'))<10^-0));

%Plot the vertical line on the small plot for reference
selectedWavelength = get (handles.slider1, 'Value');
axes(handles.axes2);
cla;
plot(handles.wavelength,log(handles.intensity(:,1)),'LineWidth',2);
set(gca,'ytick',[]);
axis([min(handles.wavelength) max(handles.wavelength) log(handles.maxI/4) log(handles.maxI+1)]);
hold on;
x1=[selectedWavelength,selectedWavelength];
y1=[0,handles.maxI];
plot(x1,y1);



x = handles.position(:,2);
y = handles.position(:,3);
z = handles.intensity(wvl_idx,:)';



x_unique = sort(unique(x));
y_unique = sort(unique(y));
offset = 0.5*(x_unique(2)-x_unique(1));


    M=zeros(length(y_unique),length(x_unique));
    for i=1:length(z)
   xi=find(y_unique==y(i));
   yi=find(x_unique==x(i));
   M(xi,yi)=z(i);
    end
    
    



   

%controlling the color scale
popupColorScale = get(handles.listbox2, 'Value');
switch popupColorScale
    
    case 1 %automatic scaling

colorMin=0; %value assiciated with color min


colorMax=handles.maxI; % z value associated with color max

    case 2

colorMin=str2double(get(handles.minColor, 'String'));
colorMax=str2double(get(handles.maxColor, 'String'));

end

% %%%%Plot
axes(handles.axes1);
cla;    

scale = get(handles.listbox3, 'Value');
switch scale
    case 1
mesh(x_unique-offset,y_unique-offset,M);
pcolor(x_unique-offset,y_unique-offset,M);
shading flat;
colorbar;
axis equal;
caxis([colorMin colorMax]);

    case 2
       mesh(x_unique-offset,y_unique-offset,log(M+1));
        pcolor(x_unique-offset,y_unique-offset,log(M+1));
        shading flat;
colorbar;
axis equal;
%caxis([log(colorMin+1) log(colorMax+1)]);

end

shading flat;
colorbar;
axis equal;
caxis([colorMin colorMax]);


handles.wvl_idx = wvl_idx;
guidata(hObject, handles);







% --- Executes during object creation, after setting all properties.
function slider1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to slider1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: slider controls usually have a light gray background.
if isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor',[.9 .9 .9]);
end


% --- Executes on selection change in listbox1.
function listbox1_Callback(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox1 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox1


% --- Executes during object creation, after setting all properties.
function listbox1_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox2.
function listbox2_Callback(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox2 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox2


% --- Executes during object creation, after setting all properties.
function listbox2_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function minColor_Callback(hObject, eventdata, handles)
% hObject    handle to minColor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of minColor as text
%        str2double(get(hObject,'String')) returns contents of minColor as a double


% --- Executes during object creation, after setting all properties.
function minColor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to minColor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end



function maxColor_Callback(hObject, eventdata, handles)
% hObject    handle to maxColor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of maxColor as text
%        str2double(get(hObject,'String')) returns contents of maxColor as a double


% --- Executes during object creation, after setting all properties.
function maxColor_CreateFcn(hObject, eventdata, handles)
% hObject    handle to maxColor (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on selection change in listbox3.
function listbox3_Callback(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: contents = cellstr(get(hObject,'String')) returns listbox3 contents as cell array
%        contents{get(hObject,'Value')} returns selected item from listbox3


% --- Executes during object creation, after setting all properties.
function listbox3_CreateFcn(hObject, eventdata, handles)
% hObject    handle to listbox3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: listbox controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end
