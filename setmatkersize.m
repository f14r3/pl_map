function setmarkersize( src, evnt )
% % trial 1 works with sermarkersize 1
%     % # get position of the figure (pos = [x, y, width, height]) 
%     pos = get(src, 'Position'); 
%     % # get the scattergroup object 
%     h = get(get(src,'children'),'children'); 
%     % # resize the marker
%     %relativesize = 5;
%     %set(h,'SizeData', pos(4)*relativesize);   
%     % # LY: resize the screen do not preserve the aspect ratio, not suitable for square
% 
%     s=6.3;
%     currentunits = get(gca,'Units');
%     set(gca, 'Units', 'Points');
%     axpos = get(gca,'Position');
%     set(gca, 'Units', currentunits);
%     markerWidth = s/diff(xlim)*axpos(3); % Calculate Marker width in points
%     set(h, 'SizeData', markerWidth^2)
    
% trial 2
    h = get(get(src,'children'),'children'); 
    s = 6.3;
    currentunits = get(gca,'Units');
    set(gca, 'Units', 'Points');
    axpos = get(gca,'Position');
    set(gca, 'Units', currentunits);

    markerWidth = s/diff(xlim)*axpos(3);
    set(h, 'SizeData', markerWidth^2)
end
